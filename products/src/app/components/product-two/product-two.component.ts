import { Component, OnInit } from '@angular/core';
import {Product} from "../../models";
import {ProductService} from '../../service';

@Component({
  selector: 'app-product-two',
  templateUrl: './product-two.component.html',
  styleUrls: ['./product-two.component.css']
})
export class ProductTwoComponent implements OnInit {

    products: Product[];
    show: boolean = true;

    constructor(private productService: ProductService) { }

    getProducts(): void {
        this.productService.getProducts().subscribe( products => {
            this.products = products;
        });
    }

    ngOnInit() {
        this.getProducts();
    }

    onShow() {
        this.show = !this.show;
    }
}
