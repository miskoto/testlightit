import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';

@Component({
    selector: 'app-registration',
    templateUrl: './registration.component.html',
    styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

    authReactiveForm: FormGroup;


    constructor(private fb: FormBuilder) {
    }

    ngOnInit() {
        this.initForm();
    }

    onSubmit() {
        const controls = this.authReactiveForm.controls;

        //Проверем форму на валидность
        if (this.authReactiveForm.invalid) {
            // Если форма не валидна, то помечаем все контролы как touched
            Object.keys(controls).forEach(controlName => controls[controlName].markAsTouched());
            return
        }
        //Обработка данных формы

        console.log(this.authReactiveForm.value);
    }

    //Инициализация формы

    private initForm() {
        this.authReactiveForm = this.fb.group({
            username: ['', [Validators.required, Validators.pattern(/[A-z]/)]],
            email: ['', [Validators.required, Validators.email]],
            password: ['']
        });
    }

 /*   isControlInvalid(controlName: string): boolean {
        const control = this.authReactiveForm.controls[controlName];
        const result = control.invalid && control.touched;
        return result;
    }*/


}
