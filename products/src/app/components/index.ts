export {ProductOneComponent} from '../components/product-one/product-one.component';
export {ProductTwoComponent} from '../components/product-two/product-two.component';
export {RateComponent} from '../components/rate/rate.component';
export {RegistrationComponent} from '../components/registration/registration.component';
export {UserComponent} from '../components/user/user.component';


