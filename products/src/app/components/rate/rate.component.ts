import { Component, OnInit } from '@angular/core';
import {Rate} from "../../models";
import {RateService} from "../../service/rate.service";
@Component({
  selector: 'app-rate',
  templateUrl: './rate.component.html',
  styleUrls: ['./rate.component.css']
})
export class RateComponent implements OnInit {

  task = {
    title: '',
    text: '',
    id: 0
  };
  tasks = [];
  rates: Rate[];

  constructor(private rateService: RateService) { }

  getRate(): void {
    this.rateService.getRate().subscribe( rates =>{
      this.rates = rates;
    });
  }

  ngOnInit() {
    this.getRate();
  }

  onAdd() {
    this.tasks.push(this.task);
  }

  onDelete(item) {
    for(let i = 0; i < this.tasks.length; i++) {
      if(item.id === this.tasks[i].id) {
        this.tasks.splice(i, 1);
        break;
      }
    }
  }

}
