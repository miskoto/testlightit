import { Component, OnInit } from '@angular/core';
import { User } from '../../models';
import { UserService } from '../../service';
import {HttpClient} from '@angular/common/http';

interface ProductResponse {
  id: number,
  title: string,
  text: string

}

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

/*
  users: User[] = [];
*/

  constructor(private http: HttpClient) { }

  ngOnInit() {

/*
    this.userService.getData().subscribe(data => this.users = data['userList']);
*/
    this.http.get<ProductResponse>('http://smktesting.herokuapp.com/',  {responseType: 'json'}).subscribe(data => {
      console.log( "id" + data.id);
      console.log("title" + data.title);
      console.log("text" + data.text);

    });

  }



}
