import { Component, OnInit } from '@angular/core';
import {ProductService} from '../../service';
import {Product} from "../../models";

@Component({
  selector: 'app-product-one',
  templateUrl: './product-one.component.html',
  styleUrls: ['./product-one.component.css']
})
export class ProductOneComponent implements OnInit {

  products: Product[];
  show: boolean = true;

  constructor(private productService: ProductService) { }

    getProducts(): void {
        this.productService.getProducts().subscribe( products => {
            this.products = products;
        });
    }

  ngOnInit() {
      this.getProducts();
  }

  onShow() {
    this.show = !this.show;
  }
}
