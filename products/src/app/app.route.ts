import {Routes} from '@angular/router';
import {RegistrationComponent, ProductOneComponent, ProductTwoComponent, UserComponent} from './components';


export const route: Routes = [
    {path: 'product_one', component: ProductOneComponent},
    {path: 'product_two', component: ProductTwoComponent},
    {path: 'registration', component: RegistrationComponent},
    {path: 'user', component: UserComponent},
    {path: '', redirectTo: '/', pathMatch: 'full'}
];

