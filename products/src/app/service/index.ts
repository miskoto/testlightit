export {ProductService} from '../service/product.service';
export {RateService} from '../service/rate.service';
export {UserService} from '../service/user.service';

/*
export {InMemoryDataService} from '../service/in-memory-data.service';
*/
