import { Injectable } from '@angular/core';
import {Rate} from "../models";
import {RATES} from '../mock-rate';
import {Observable, of} from "rxjs/index";

@Injectable({
  providedIn: 'root'
})
export class RateService {


  constructor() { }

  getRate(): Observable<Rate[]> {
    return of (RATES);
  }

}

