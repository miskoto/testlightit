import {Injectable} from '@angular/core';
import {Product} from "../models";
import {PRODUCTS} from '../mock-products';
import {Observable, of} from "rxjs/index";

@Injectable({
    providedIn: 'root'
})
export class ProductService {

    constructor() {

    }

    getProducts(): Observable<Product[]> {
        return of (PRODUCTS);
    }

    /*getProduct(id: number): Observable<Products[]> {
        const url = `${this.productsUrl}\${id}`;
        return this.http.get<Products>(url).pipe(
            tap(() => this.log(`fatched product id=${id}`)))

    };

    updateProduct(product: Product): Observable<any> {
        return this.http.put(this.productsUrl, product, httpOptions).pipe(
            tap(() => this.log(`update product id=${id}`)))

};*/

}
