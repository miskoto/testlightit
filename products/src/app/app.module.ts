import {BrowserModule} from "@angular/platform-browser";
import { NgModule } from '@angular/core';
import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule} from '@angular/router';
import {MatCardModule} from '@angular/material/card';
import {FormsModule} from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';


import { AppComponent } from './app.component';
import { RegistrationComponent, ProductTwoComponent, ProductOneComponent, RateComponent, UserComponent } from './components';
import {route} from "./app.route";
import {ProductService, RateService, UserService} from "./service";
import {HttpClientModule} from "@angular/common/http";
import {HttpClientInMemoryWebApiModule} from "angular-in-memory-web-api";



@NgModule({
  declarations: [
    AppComponent,
    ProductTwoComponent,
    RegistrationComponent,
    RateComponent,
    ProductOneComponent,
    UserComponent,
  ],
  imports: [
    BrowserModule,
    MatButtonModule,
    MatCheckboxModule,
    BrowserAnimationsModule,
    MatCardModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
/*    HttpClientInMemoryWebApiModule.forRoot(
        InMemoryDataService, {dataEncapsulation: false}
    ),*/
    RouterModule.forRoot(
        route,
        { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [
      ProductService,
      RateService,
      UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
