import {Product} from "./models";

export const PRODUCTS: Product[] = [
    {
        id: 1,
        title: "Apple",
        text: "Some quick example text to build on the card title and make up the bulk of the card's content."
    },
    {
        id: 2,
        title: "Carrot",
        text: "Some quick example text to build on the card title and make up the bulk of the card's content."
    },

];