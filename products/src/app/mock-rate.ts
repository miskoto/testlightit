import {Rate} from "./models";

export const RATES: Rate[] = [
    {
        id: 1,
        id_product: 2,
        title: 'Carrot',
        text: "Some quick example text to build on the card title and make up the bulk of the card's content."
    },
    {
        id: 2,
        id_product: 1,
        title: 'Apple',
        text: "Some quick example text to build on the card title and make up the bulk of the card's content."
    },

];